Aplicativo para controle financeiro pessoal desenvolvido com base no livro "Programação Java para a Web" da editora Novatec

Tecnologias usadas no desenvolvimento do sistema:

    - Java
    - JavaServer Faces
    - Facelets e Primefaces
    - Hibernate
    - MySql
    - Maven
    - Segurança com Spring Security
    - Conexão com portais externos para gerenciamento de Ações (Yahoo Finance)
    - Gráficos com Primefaces
    - Relatórios com iReport e Jasper Reports
